# Python Academy Project

Python is an extremly versatile language. In this project you'll use python to create a simple API end point and host it. You'll also ensure new change happen because your going to take effect add a simple CI / CD. AH! and you'll host your own unit tests. Which you can test on your CI.

Topics covered:
- Git and bitbucket
- Markdown
- 

### Getting Started

Please read each user story and start sprint planning. Add acceptance criterea to each story and then plan your sprints.

Remember to finalise by making sure the DoD is done.

### User Stories

**User story 1** As a front end developer, I want to have a muffin api that has an endpoint `/muffins` supplies me with names of 10 muffins, so that I can get only the names and display them.

**User story 2** As a front end developer, I want to have a muffin api that has an endpoint `/prices` supplies me with 10 muffins prices, so that I can make display them on the website.

**User story 3** As a front end developer, I want to have a muffin api that has an endpoint `/products` supplies me with all the information about the 10 muffins in a JSON, so that I can make display them on the website picture and price.

**User story 4** As a developer, I want each api endpoint to have at least 2 test, so that I know the API is working and so I can used them in my testing.

**User story 5** As a developer, I want each api endpoint to have at least 2 test, so that I know the API is working and so I can used them in my testing.

**User story 6** As a developer, I want my code to be automatically tested when I push to branch `DEV-TEST-*`, so that I know if my latest changes have worked.

**User story 7** As a developer, if the tests have passed, I want to be informed on slack, so that I am in the know.

**User story 8** As a developer, if the tests have passed I want the pipeline to marge, so that the code is up to date.

**User story 9** As a developer, I want to have the API running on a server in a cloud provider and to be able to consume the API anywhere in the world, so that my front end devs can connect to it.

**User story 10** As a developer, new update to the API to be live as soon as possble.

#### Extra Possible stories

**DB extra**

- Make a DB that for the Muffins. A muffin should have a Type and Price
- Use pyodbc or another python package to connect you mysql to a Python file
- Make methdos to CRUD - Creat Read Update Delete


**ADD terraform to it**
Add a terraform to deploy your app, plus it's environment + it's testing environment
